# mp3-organiser - rename mp3 files using tag data.

Uses a configuration file to specify how the path and filename of an mp3 file is replaced with data from the tags.

## Configuration File

The configuration file is in configparser (.ini file) format and specifies where to look for files, where to put them, and how to name them.

### Default Section

The ```[default]``` section contains: source Where to look for the MP3 files

#### destination

How to rename/move the files - this is the path with various parts replaced with tag expressions enclosed in '<' and '>'.

A tag expression consists of the name of an MP3 tag chosen from 'album', 'artist', 'song', 'track', 'year', 'genre', 'band', or 'composer'. By default, the tag expressions from the destination are replaced with the tag values from the file, but each tag can have a separate configuration section describing how the tag should be formatted (see below).

**Example:**

```destination = /storage/audio/<genre>/<album>/<track><song>.mp3```

would rename/move each MP3 file into the /storage/audio directory with genre and album subdirectories and the file named as the song name preceded by the track number.

#### pattern
The wildcard used to search for MP3 files (defaults to '*.mp3')

#### recurse
Set to 'true' to process the source directory and subdirectories

#### dry-run
Set to 'true' to report what would be done without doing it.

#### verbose
Run in verbose mode where of additional output is produced

#### debug
Run in debug mode where lots of additional output is produced (very verbose)

### Tag-Format Configuration

The configuration file can contain zero or more tag-formatting configuration sections, each of which is named for the tag ('album', 'artist', 'song', 'track', 'year', 'genre', 'band', or 'composer') and specifies how the tag should be formatted when it is inserted into the file name or path.

There are just two configuration items that can occur in the tag-formatting data:

#### optional
If set to True, the tag will be treated as an empty string if it is not present in the file. Otherwise if the destination path references the tag and the file does not contain it, an error will be reported.

#### format:
Specifies how the tag should be formatted using a C-style printf formatter for string or integer values (track numbers only).

## Command Line

Some of the configuration file options can be overridden on the command line. The command line format is:

```usage: mp3-organiser [-h] [-D] [-r] [-s SOURCE] [-p PATTERN] [-d DESTINATION] [--debug] [--verbose]```

## Optional arguments:
### -c CONFIGFILE, --config CONFIGFILE
Specify the name and location of the configuration file.
### -h, --help
show this help message and exit.
### -D, --dry-run
Dry-run - report what would be done without doing anything.
### -r, --recurse
Operate recursively from the source directory.
### -s SOURCE, --source SOURCE
Specify the source directory.
### -p PATTERN, --pattern PATTERN
Specify the file search pattern (default=*.mp3).
### -d DESTINATION, --destination DESTINATION
Specify the destination directory, including tags.
### --debug
Output debug information.
### --verbose
Output verbose information.

# Notes

In order to avoid filenames that might be invalid on some filesystems (particularly NTFS or MS-DOS) some characters in tags are removed if they occur prior to being used to create the destination filename and/or path. These are:

* '?.' is replaced with '.'
* '/', '?', '*' and ':' are replaced with '-'
* '\', '<' and '>' are removed
